﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class GameplayController : MonoBehaviour {

	public static GameplayController instance;

	private Text ScoreText;
	private Text LifeText;

	private int score;
	private int lifeScore;

	void Awake () {
		MakeInstance ();

		ScoreText = GameObject.Find ("ScoreText").GetComponent<Text> ();
		LifeText = GameObject.Find ("LifeText").GetComponent<Text> ();	
	}

	void OnEnable(){	
		SceneManager.sceneLoaded += LevelFinishedLoading;
	}

	void OnDisable(){
		SceneManager.sceneLoaded -= LevelFinishedLoading;
	}

	void LevelFinishedLoading (Scene scene, LoadSceneMode mode){
		if (scene.name == "Level1") {
			if (!GameManager.instance.playerDiedGameRestarted) {
				score = 0;
				lifeScore = 2;
			} else {
				score = GameManager.instance.score;
				lifeScore = GameManager.instance.lifeScore;
			}
			ScoreText.text = "x" + score;
			LifeText.text = "x" + lifeScore;
		}
	}

	private void MakeInstance() {
		if (instance == null) {
			instance = this;
		}
	}

	public void IncrementScore() {
		score++;
		ScoreText.text = "x" + score;
	}

	public void DecrementLife(){
		lifeScore--;
		if (lifeScore >= 0) {
			LifeText.text = "x" + lifeScore;
		}
		StartCoroutine (PlayerDied ());
	}

	IEnumerator PlayerDied () {
		yield return new WaitForSeconds (2f);

		if (lifeScore < 0) {
			SceneManager.LoadScene ("Main Menu");
		} else {
			GameManager.instance.playerDiedGameRestarted = true;
			GameManager.instance.score = score;
			GameManager.instance.lifeScore = lifeScore;
			SceneManager.LoadScene ("Level1");
		}
	}
}//class
