﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

	public void StartGame(){
		SceneManager.LoadScene ("Level1");	
	
	}
	public void Instructions(){
		SceneManager.LoadScene (2);
	}

	public void QuitGame (){
		UnityEditor.EditorApplication.isPlaying = false;
	}
} //class
