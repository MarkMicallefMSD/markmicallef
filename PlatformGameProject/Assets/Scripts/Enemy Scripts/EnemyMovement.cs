﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyMovement : MonoBehaviour {

	private float speed = 2f;
	public bool walkLeft;

	void Start () {
		StartCoroutine (ChangeDirection());
	}

	void Update () {
		Walk ();
	}

	private void Walk() {
		Vector3 temp = transform.position;
		Vector3 tempScale = transform.localScale;
		if (walkLeft) {
			temp.x -= speed * Time.deltaTime;
			tempScale.x = -Mathf.Abs (tempScale.x);
		}else{
			temp.x += speed * Time.deltaTime;
			tempScale.x = Mathf.Abs (tempScale.x);
		}
		transform.position = temp;
		transform.localScale = tempScale;
}
	IEnumerator ChangeDirection (){
		yield return new WaitForSeconds (3f);
		walkLeft = !walkLeft;
		StartCoroutine (ChangeDirection ());
	}

	void OnTriggerEnter2D(Collider2D res){
		if (res.CompareTag ("Player")) {
			Destroy (res.gameObject);
			SceneManager.LoadScene (3);
		}
	}
	
}//class
