﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	private float speed = 30f, maxVelocity = 4f;
	private Animator anim;

	private Rigidbody2D myBody;

	private bool isGrounded = false;
	private float jumpForce = 700f;

	void Awake() {
		myBody = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}

	void FixedUpdate () {
		PlayerWalkKeyboard();
		Jump ();
	}

	private void PlayerWalkKeyboard() {
		float forceX = 0f;
		float vel = Mathf.Abs (myBody.velocity.x);

		float h = Input.GetAxisRaw ("Horizontal");

		if (h > 0) {
			if (vel < maxVelocity) {
				forceX = speed;
			} 
			Vector3 temp = transform.localScale;
			temp.x = 3.07f;
			transform.localScale = temp;
			anim.SetBool ("Walk", true);

		}  else if (h < 0){
			if (vel < maxVelocity) {
				forceX = -speed;
				Vector3 temp = transform.localScale;
				temp.x = -3.07f;
				transform.localScale = temp;
				anim.SetBool ("Walk", true);
			}
			} else {
			
				anim.SetBool ("Walk", false);
			}

		
		myBody.AddForce (new Vector2 (forceX, 0));
		}

		private void Jump() {
			if (Input.GetKey(KeyCode.Space)) {
				if (isGrounded) {
				isGrounded = false;
				myBody.AddForce (new Vector2 (0, jumpForce));
		
					}
				}
			}
		void OnTriggerEnter2D (Collider2D target)
		{
			if (target.tag == "Ground") {
			isGrounded = true;
		}
	}
} //class
