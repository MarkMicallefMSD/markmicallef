﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerScore : MonoBehaviour {

	public bool isAlive;
	private gameMaster gm;
	private GameObject gameFinishedText;

	public int HealthCurrent;
	public int MaxHealth = 5;
	void Update(){
		  
		if (HealthCurrent > MaxHealth) {
			HealthCurrent = MaxHealth;
			}

		if (HealthCurrent <= 0) {
			Die ();
			}
		}
	void Awake () {
		isAlive = true;
		gameFinishedText = GameObject.Find ("LevelFinished");
		gameFinishedText.SetActive (false);
		gm = GameObject.FindGameObjectWithTag ("GameMaster").GetComponent<gameMaster> (); 

		HealthCurrent = MaxHealth; 
	}

	void OnTriggerEnter2D(Collider2D col){
	
		if (col.CompareTag ("Coin")) {
			Destroy (col.gameObject);
			FindObjectOfType<AudiosManager> ().Play ("Coin");
			gm.points += 1;
		}
		if (col.tag == "Enemy") {
			if (isAlive) {
				isAlive = false;
				transform.position = new Vector3 (0, 10000, 0);
			}
		}

		if (col.tag == "Exit") {
			gameFinishedText.SetActive (false);
			Time.timeScale = 0f;
		}
	}
		void Die(){
			
			Application.LoadLevel (Application.loadedLevel);
		
		}

} //class
