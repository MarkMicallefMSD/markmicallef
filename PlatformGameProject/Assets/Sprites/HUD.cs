﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

	public Sprite[] HeartSprites;

	public Image HearthUI;

	private PlayerScore Player;

	void Awake (){
	
		Player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerScore> ();
	}

	void Update (){
		HearthUI.sprite = HeartSprites[Player.HealthCurrent];
	}


}
